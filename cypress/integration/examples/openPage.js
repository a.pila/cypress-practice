describe('First test', function()
{
    it('Verify title of the page', function()
    {
        cy.visit('https://www.emag.ro/')
        cy.title().should('eq', 'eMAG.ro - Libertate în fiecare zi')
    })
})